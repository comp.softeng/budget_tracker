import React from 'react';
import { Fragment ,useState, useEffect, useContext} from 'react'; 
import { Navbar, Nav } from 'react-bootstrap';
import Link from 'next/link'; 
import UserContext from '../userContext'

export default () => {
	const {user} = useContext(UserContext)
	const [isExpanded, setIsExpanded] = useState(false)

	let RightNavOptions
	let LeftNavOptions

	if(user.email !== null){
		RightNavOptions = (
			<Fragment>
				<Link href='/logout'>
					<a className='nav-link'>
						Logout
					</a>
				</Link>
			</Fragment>
		)
		LeftNavOptions = (
			<Fragment>
				<Link href='/user/categories'>
					<a className='nav-link'>
						Categories
					</a>
				</Link>
				<Link href='/user/records'>
					<a className='nav-link'>
						Records
					</a>
				</Link>
				<Link href="/user/charts/category-breakdown">
					<a className='nav-link'>
						Breakdown
					</a>
				</Link>
			</Fragment>
		)
	} else {
		RightNavOptions = null
		LeftNavOptions= null
	}

   return(
   	<Fragment>
	   	<Navbar expanded={isExpanded} expand="lg" fixed="top" variant="dark" className='navbarBg'>
	   	 <Link href="/">
	   	     <a className="navbar-brand">Money Wise</a>
	   	 </Link>
	   	 <Navbar.Toggle onClick={() => setIsExpanded(!isExpanded)} aria-controls="basic-navbar-nav" />
	   	 <Navbar.Collapse className="basic-navbar-nav">
	        <Nav className="mr-auto" onClick={() =>  setIsExpanded(!isExpanded)}>
				{RightNavOptions}
	        </Nav>
			<Nav className="ml-auto" onClick={() =>  setIsExpanded(!isExpanded)}>
				{LeftNavOptions}
	        </Nav>
	   	 </Navbar.Collapse>
	   	</Navbar>
   	</Fragment>
   	)
}
