import { useState } from 'react'
import { Form, Button, Row, Col, Card } from 'react-bootstrap'
import Router from 'next/router'
import Swal from 'sweetalert2'
import View from '../../../components/View';
import AppHelper from '../../../app-helper';


const NewCategoryForm = () => {
        const [categoryName, setCategoryName] = useState('')
        const [typeName, setTypeName] = useState(undefined)

const createCategory = (e) => {
    e.preventDefault()
    const payload = {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${AppHelper.getAccessToken()}`
        },
        body: JSON.stringify({
            name: categoryName,
            type: typeName
        })  
    }
    fetch(`${AppHelper.API_URL}/users/add-category`,payload).then(res => res.json()).then(data => {
        if(data == true){
            Swal.fire({
                icon:'success',
                title:'Category Added',
                background: '#020025'
            })
        } else {
            Swal.fire({
                icon:'error',
                title: 'Awww, Snap',
                text: "Something went wrong in adding", 
                background: '#020025',
            })
        }
    })  
}
    return (

        <View title={ 'Money Wise | Add Category' }>     
        <Form onSubmit={(e) => createCategory(e)} className= 'categoriesForm'>
            <Form.Group controlId="categoryName">
                <Form.Label>Category Name:</Form.Label>
                <Form.Control type="text" placeholder="Enter category name" value={ categoryName } onChange={ (e) => setCategoryName(e.target.value) } required/>
            </Form.Group>
            <Form.Group controlId="typeName">
                <Form.Label>Category Type:</Form.Label>
                <Form.Control as="select" 
                value={typeName} 
                onChange={(e) => setTypeName(e.target.value)} required>
                    <option value selected disabled>Select Category</option>
                    <option value="Income">Income</option>
                    <option value="Expense">Expense</option>
                </Form.Control>
            </Form.Group>
            <Button type="submit">Add</Button>
        </Form>
    </View>

    )
}
export default NewCategoryForm;
