import { useState, useEffect } from 'react'
import { InputGroup, Form, Col } from 'react-bootstrap'
import { Pie } from 'react-chartjs-2'
import View from '../../../components/View'
import moment from 'moment'
import randomColor from 'randomcolor'
import AppHelper from '../../../app-helper'


const CategoryBreakdrown = () => {
    const [labelsArr, setLabelsArr] = useState([])
    const [dataArr, setDataArr] = useState([])
    const [bgColors,setBgColors] = useState([])
    const [fromDate, setFromDate] = useState(moment().subtract(1, 'months').format('YYYY-MM-DD'))
    const [toDate, setToDate] = useState(moment().format('YYYY-MM-DD'))

const data = {
        labels: labelsArr,
        datasets: [
            {
                data: dataArr,
                backgroundColor: bgColors
            }
        ]
    };
    useEffect(() => {
        const payload = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${ AppHelper.getAccessToken() }`
            }, 
            body: JSON.stringify({
                fromDate: fromDate,
                toDate: toDate
            })    
        }
fetch(`${AppHelper.API_URL}/users/get-records-breakdown-by-range`, payload)
      .then((res) => res.json())
      .then((records) => {
        setLabelsArr(records.map((record) => record.categoryName));
        setDataArr(records.map((record) => record.totalAmount));
        setBgColors(records.map(() => randomColor()));
      });
    }, [fromDate, toDate])
    return (

        <View title="Money Wise | Category Breakdown">
            <h3>Category Breakdown</h3>
            <Form.Row>
                <Form.Group as={ Col } xs="6">
                    <Form.Label>From</Form.Label>
                    <Form.Control type="date" value={ fromDate } onChange={ (e) => setFromDate(e.target.value) }/>
                </Form.Group>
                <Form.Group as={ Col } xs="6">
                    <Form.Label>To</Form.Label>
                    <Form.Control type="date" value={ toDate } onChange={ (e) => setToDate(e.target.value) }/>
                </Form.Group>
            </Form.Row>
            <hr/>
            <Pie data={data}/>
        </View>

    )
}
export default CategoryBreakdrown;
