import React from 'react';
import { useState, useContext } from 'react'
import { Form, Button, Card, Row, Col } from 'react-bootstrap'
import styles from '../styles/Home.module.css'
import UserContext from '../userContext';  
import View from '../components/View'; 
import Swal from 'sweetalert2'
import Router from 'next/router'
import Image from 'next/image'
import AppHelper from '../app-helper'

const Home= () => {
 const [email, setEmail] = useState("")
 const [password, setPassword] = useState("")
 const { user, setUser } = useContext(UserContext)
   if(user.email !== null) {
    Router.push('/user/records');
   }
  //lets create a function to retrieve the user details before the client can be redirected inside the rECORDS page. 
  const retrieveUserDetails = (accessToken) => {
     //we have to make sure the user has already been verified meaning it was already granted access token.
     const options = {
         headers: { Authorization: `Bearer ${accessToken}`}
     }
     //create a request going to the desired enspoint with the payload.
     fetch(`${AppHelper.API_URL}/users/userDetails`, options).then((response) => response.json()).then(data => {
         setUser({ email: data.email }) //lets acquiring the email property from the data res. 
         Router.push('/user/records') 
     })
  }

  function login(e){
    e.preventDefault()
    //describe the request to login
    fetch(`${AppHelper.API_URL}/users/login`, {
    	method: 'POST',
    	headers: {
    		'Content-Type': 'application/json'
    	},
    	body: JSON.stringify({
    		email: email,
    		password: password
    	}) //for the request to be accepted by the api
    }).then(res => res.json()).then(data => {
    	//lets create a control structure
    	if(typeof data.accessToken !== "undefined") {
    		//store the access token in the local storage
    		localStorage.setItem('token', data.accessToken)
        retrieveUserDetails(data.accessToken)

        Swal.fire({
          icon: 'success',
          title: 'Welcome to your Dashboard!',
          background: '#020025'
        })
     
      } else {
        Swal.fire({
          icon: 'error',
          title: 'Login Error',
          text: 'You may have registered using a different login procedure',
          background: '#020025'
        })
      }
    })
  }
  return (

      <View title={ 'Money Wise | Login' }>
            <Row>
              <Col sm={8}>
                <h1 className='login'>Welcome, Wise!</h1>
                <p className='login'>Login using your registered email. <b><a href='/register'>Not yet registered?</a></b></p>
                <Form onSubmit={e => login(e)} className='loginForm'>
                    <Form.Group controlId="email">
                        <Form.Label>Email:</Form.Label>
                        <Form.Control type="text" value={email} onChange={e=>setEmail(e.target.value)} required/>
                    </Form.Group>
                    <Form.Group controlId="password">
                        <Form.Label>Password:</Form.Label>
                        <Form.Control type="password" value={password} onChange={e=>setPassword(e.target.value)} required/>
                    </Form.Group>
                    <Button type="submit" className="btn">Sign in</Button>
                </Form>
              </Col>
              <Col sm={4}>
                  <Image src='/4281995.jpg' height={600} width={600}/>
              </Col>
            </Row>
      </View>

  )
}
export default Home;